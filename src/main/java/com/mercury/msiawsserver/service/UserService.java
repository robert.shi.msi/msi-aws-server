package com.mercury.msiawsserver.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mercury.msiawsserver.bean.User;
import com.mercury.msiawsserver.dao.UserDao;

@Service
@Transactional
public class UserService {
	
	@Autowired
	UserDao userDao;
	
	public List<User> getAllUsers() {
		return userDao.findAll();
	}

}
