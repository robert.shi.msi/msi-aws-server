package com.mercury.msiawsserver.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.msiawsserver.bean.User;

public interface UserDao extends JpaRepository<User, Long> {

}
